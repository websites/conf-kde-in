---
title: Home
---

# conf.kde.in

## 2025 in Gandhinagar, Gujarat

conf.kde.in aims to attract new KDE Community members, as well as seasoned developers. The contents of the conference provide updates on what is going on in the KDE Community and teaches newcomers how to start making meaningful contributions.

{{< container class="pt-1 pb-4 text-center" >}}

![](/media/2024.jpg)
*conf.kde.org 2024*  
<small>by KDE India Team <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="/media/2024/ccby-wee.png"></a></small>

{{</ container >}}

This event attracts speakers from all over India. It provides students with an excellent opportunity to interact with established open-source contributors, as well as developers from various industries working on open-source projects in the fields of automotive, embedded, mobile, and more.

conf.kde.in was started in 2011 at the R V College of Engineering, Bangalore by a group of Indian KDE contributors. Since then we have hosted six different editions, each in different universities and venues:

- 2013, KDE Meetup, DA-IICT, Gandhinagar
- conf.kde.in 2014, DA-IICT, Gandhinagar
- conf.kde.in 2015, Amrita University, Kerala
- conf.kde.in 2016, LNMIIT, Jaipur
- conf.kde.in 2017, IIT Guwahati
- conf.kde.in 2020, Maharaja Agrasen Institute of Technology, Delhi
- conf.kde.in 2024, COEP Technological University, Pune

Past events have been successful in attracting Indian students to mentoring programs such as Google Summer of Code (GSoC), Season of KDE, and Google Code-In. These programs have often successfully helped students become lifelong contributors to open-source communities, including KDE.

**[conf.kde.in 2025](/2025) will be an offline event, in Gandhinagar, Gujarat from Friday 4th - Sunday 6th April**
