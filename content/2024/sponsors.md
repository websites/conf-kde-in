---
title: Sponsors
menu:
  "2024":
    weight: 5
---

## Sponsors for conf.kde.in

{{< sponsor homepage="https://foss.coep.org.in/cofsug" img="/media/cofsug.png" >}}

<p id="tqc">The <strong>COEP's Free Software Users Group (CoFSUG)</strong> is a volunteer group at the COEP Technological University that promotes the use and development of free and open source software. CoFSUG runs the FOSS Lab, FOSS Server, COEP Moodle, COEP LDAP Server, and COEP Wiki. The group carries out activities like installation festivals to teach GNU/Linux, workshops on Linux administration, Python, Drupal, and FOSS technologies, promoting software development under the GNU GPLv3 license, and Freedom Fridays to spread FOSS philosophy. It has in the past hosted the Fedora Users and Developers Conference 2011, Android app development workshops, spoken tutorials program from IIT Bombay, and summer coding projects for students on FOSS contributions. CoFSUG organizes FLOSSMEET, a flagship event to create awareness and encourages the use of free and open-source software. Previous editions hosted talks on technologies such as Docker, Kubernetes, open-source licenses, and OSINT, exploring RUST to name a few.

Overall, CoFSUG aims to advance the use of free and open source solutions in academia through hands-on training, student projects, and evangelism about the FOSS philosophy.
<p>

<https://foss.coep.org.in/cofsug> 

{{< /sponsor >}}

### KDE Patrons
[KDE Patrons](http://ev.kde.org/supporting-members.php) also support the KDE Community throughout the year.
