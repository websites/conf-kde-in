---
title: Venue
menu:
  "2025":
    weight: 4
---

conf.kde.in is hosted at the DA-IICT, Gandhinagar, Gujarat.



DA-IICT is spread over 50 acres of land in Gandhinagar, Capital City of Gujarat. The DA-IICT campus is caringly planned and designed as an environmentally conscious campus in the country. The architecture of DA-IICT is functional, but what surrounds it is a fascinating garden. The entire design is oriented towards preserving the environment. The campus with trees, lawns and bushes bearing green leaves and exotic flowers surrounding the buildings and pathways instils environment consciousness among students and enrich their learning. The campus also has a herb garden with species of rare medicinal plants. The landscape was planned and developed in a manner that no rainwater is lost. The irrigation for campus garden and lawns is carried out with recycled water. Its solid waste management system churns out organic fertilizer out of dry leaves, vegetable and food waste generated from food courts.

The campus is a haven for bird-watchers, with a variety of species of birds being spotted.

DA-IICT can be reached in about 30 minutes from Sardar Vallabhai Patel International Airport and the Central Railway Station located in Ahmedabad.

**Address:** Near Indroda Circle, Gandhinagar, Gujarat 382007, India
[**OpenStreetMap**](https://www.openstreetmap.org/way/398144592#map=17/23.188377/72.628158) - [**Google maps**](https://maps.app.goo.gl/Q5FxPwfNYU9gptN86)

**Website:** <https://www.daiict.ac.in/>
