---
title: Sponsors
menu:
  "2025":
    weight: 5
---

## Sponsors for conf.kde.in 2025

{{< sponsor homepage="https://dscdaiict.in/" img="/media/gdg-on-campus.svg" >}}

<p id="gdg">The <strong>GDG on Campus Dhirubhai Ambani Institute of Information & Communication Technology - Gandhinagar, India</strong> is a group of individuals united by a strong passion for technology, it&apos;s progress, and open-source ideas. Their enthusiasm is dedicated to sharing this passion with those who aspire to explore, educate, innovate, and become involved in the same technological domain that has become an integral part of each of their lives.</p>

<https://dscdaiict.in/>

{{< /sponsor >}}

### KDE Patrons

[KDE Patrons](http://ev.kde.org/supporting-members.php) also support the KDE Community throughout the year.
